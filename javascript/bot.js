﻿var _ = require('underscore');
var keimolaspec = require('./tracks/keimola.spec');
var keimolaTestRun1 = require('./tracks/keimola.spec.testrun1');
var physicsCalculator = require('./physicsCalculator');
var switchLanesManager = require("./switchLanesManager");
var throttleManager = require('./throttleManager');
var model = require('./model');
var log = require('./logWrapper');
var competitorTracker = require('./competitorTracker');
var t = require("./throttleCalculator");

var consoleLogger = require('./consoleLogger');

module.exports = function bot() {    
    var self = this;

    self.model = model.initModel();

    var getFixedInstructionsForKeimola = function(positions, tick) {

        var currentStatus = _.find(positions, function(el) {
            return el.id.name === self.model.myCar.name;
        });

        var currentPosition = currentStatus.piecePosition;
        log.debug("current position: %j", currentPosition);

        // NOTE: change test spec here
        // TODO: could be taken as an argument
        var piece = _.find(keimolaTestRun1.instructions.pieces, function(el) {
            return el.index == currentPosition.pieceIndex;
        });

        var speed = self.getSpeed(self.model, currentPosition);
        log.debug("current speed: " + speed + " current angle: " + currentStatus.angle + " tick: " + tick + " throttle: " + piece.throttle);

        self.model.carPositions = positions;
        self.model.myStatus.position = currentPosition;

        var msg = {};

        if (!_.isUndefined(piece.switchLane)) {
            msg.msgType = "switchLane";
            msg.data = piece.switchLane;
        } else {
            msg.msgType = "throttle";
            msg.data = piece.throttle;
        };

        return msg;
    }

    var latestLaneSwitchMessage;
    var ticksSinceLastThrottle = 0;

    // next action which will be sent to server
    var getNextMessage = function(positions, tick) {

        // send throttle by default
        var message = {
            msgType: "throttle",
            data: self.model.myStatus.throttle
        };

        self.model.updateStatus(positions, tick);
        log.debug("status", self.model.myStatus);

        // start sending race msgs only after race started
        if (self.model.raceStarted) {
            var throttleCalculator = new t.ThrottleCalculator(self.model.mapPieceInfo, self.model.environmentVariables);
            var s = self.model.myStatus;
            var newThrottle = throttleCalculator.getMaxSafeThrottle(s.previousThrottle, s.position, s.velocity, s.slipAngle, s.slipAngleVelocity, s.turbo);

            if (newThrottle > 1) {
                newThrottle = 1;
            }

            // dont send throttle when crashed, only after spawning
            if (self.model.myStatus.velocity < 0.5 || (self.model.previousStatus.throttle != newThrottle && !self.model.myStatus.crashed) || (self.model.myStatus.crashed && self.model.myStatus.spawned)) {
                self.model.myStatus.crashed = self.model.myStatus.spawned = false;
                // every second time the throttle should be on, it will be sent again, because the throttle will not be copied.
                // this is OK, because the server might ignore throttle commands sent before the race has started -> the throttle won't be sent
                log.debug("Changing throttle to " + newThrottle);
                self.model.myStatus.throttle = newThrottle;

                message.msgType = "throttle";
                message.data = newThrottle;
            } else {
                var currentPosition = self.model.myStatus.position;
                self.model.myStatus.throttle = newThrottle; // keep the throttle updated in the model eventhough it has not changed

                // TODO: null checks
                var currentPiece = currentPosition.pieceIndex;
                var currentLane = currentPosition.lane.endLaneIndex;

                var switchInfo = switchLanesManager.getNextLaneChange(self.model.mapPieceInfo,
                    self.model.race.track.lanes, currentLane, currentPiece);

                // TODO: currently it will always send lane switch
                // should remember that request has already been sent for that switch position
                if (switchInfo.switchPending &&
                (!latestLaneSwitchMessage || latestLaneSwitchMessage.pieceIndex != switchInfo.pieceIndex || !latestLaneSwitchMessage.sentAt)) {
                    message.msgType = "switchLane";
                    message.data = switchInfo.direction;

                    latestLaneSwitchMessage = { msg: message, pieceIndex: switchInfo.pieceIndex };

                }
            }

            if (self.model.clearForTurbo && self.model.turbo && !self.model.turbo.startedAt && tick) {
                self.model.turbo.startedAt = tick;

                message.msgType = "turbo";
                message.data = "Gimme warp speed!";

                consoleLogger.logTurbo(self.model.turbo);
            }
        }

        if (message.msgType === "throttle") {
            ticksSinceLastThrottle = 0;
        } else {
            ticksSinceLastThrottle++;
        }

        if (message.msgType === "ping" && ticksSinceLastThrottle > 1) {
            // send throttle, just in case
            message.msgType = "throttle";
            message.data = self.model.myStatus.previousThrottle;
        } else if (message.msgType === "switchLane") {
            latestLaneSwitchMessage.sentAt = tick;
        }

        consoleLogger.logStatus(self.model.myStatus);

        return message;
    };

    // TODO: why should the bot pipe these method calls?
    var initCar = function(data) {
        self.model.initCar(data);
    }

    // TODO: why should the bot pipe these method calls?
    var initRace = function(data) {
        self.model.initRace(data);
    }
    var raceStarted = function() {
        self.model.raceStarted = true;
    };
    
    var raceEnded = function (results) {
        consoleLogger.logRaceEnd(results);
    };


    var crashed = function (data, tick) {
        if (data.name == self.model.myCar.name) {
            consoleLogger.logCrash(data);
        }
        competitorTracker.crashed(data);
        self.model.reportCrash();
        // if we have crashed, reset throttle in model so that we will get fresh start based on new calculations
        if (data.name === self.model.myCar.name) {
            self.model.myStatus.crashed = true;
            self.model.throttle = -1;
        };
    };

    var spawned = function (data, tick) {
        if (data.name === self.model.myCar.name) {
            self.model.myStatus.spawned = true;
        }
    };

    var turboAvailable = function(data) {
        self.model.turbo = {data: data}
    };

    var turboStarted = function(tick) {
        if (self.model.turbo) {
            self.model.turbo.startedAt = tick;
        }
    };

    var turboEnded = function() {
        if (self.model.turbo) {
            self.model.turbo = null;
        }
    }

    var lapFinished = function(data){
        competitorTracker.lapFinished(data);
    }

    var dnf = function(data) {
        competitorTracker.dnf(data.name);
    }

    var finished = function(data) {
        competitorTracker.finished(data);
    }

    return {
        initCar: initCar,
        initRace: initRace,
        raceStarted: raceStarted,
        raceEnded: raceEnded,
        crashed: crashed,
        spawned: spawned,
        turboAvailable: turboAvailable,
        turboStarted: turboStarted,
        turboEnded: turboEnded,
        lapFinished: lapFinished,
        finished: finished,
        dnf: dnf,
        getNextMessage: getNextMessage,
        getFixedInstructionsForKeimola: getFixedInstructionsForKeimola,
        setConstantThrottle: function(constantThrottle) { self.model.constantThrottle = constantThrottle; }
    };
}();
