/*
goal of this test run is to see how acceleration and breaking works

idea is to try start speeding up from keimola's main straight from zero speed and then
do full stop at the end of the straight
*/

var instructions = {};
instructions.pieces = [
	{
		// "length": 100
		index: 0,
		throttle: 1.0
	},
	{
		// "length": 100
		index: 1,
		throttle: 1.0
	},
	{
		// "length": 100
		index: 2,
		throttle: 0.0
	},
	{
		// 1st switch
		// "length": 100, "switch": true
		index: 3,
		throttle: 0.0
	},
	{
		// "radius": 100, "angle": 45
		index: 4,
		throttle: 0.2
	},
	{
		// "radius": 100, "angle": 45
		index: 5,
		throttle: 0.2
	},
	{
		// "radius": 100, "angle": 45
		index: 6,
		throttle: 0.5
	},
	{
		// "radius": 100, "angle": 45
		index: 7,
		throttle: 0.5
	}, 
	{	
		// 2nd switch
		// "radius": 200, "angle": 22.5, "switch": true
		index: 8,
		throttle: 0.5
	},
	{	
		// "length": 100
		index: 9,
		throttle: 0.5
	},
	{	
		// "length": 100
		index: 10,
		throttle: 0.5
	},
	{
		// "radius": 200, "angle": -22.5
		index: 11,
		throttle: 0.5
	},
	{
		// "length": 100
		index: 12,
		throttle: 0.5
	},
	{
		// 3rd switch
		// "length": 100, "switch": true
		index: 13,
		throttle: 0.5
		// ,switchLane: "Left"
	},
	{
		// "radius": 100, "angle": -45
		index: 14,
		throttle: 0.5
	},
	{
		//  "radius": 100, "angle": -45
		index: 15,
		throttle: 0.5
	},
	{
		// "radius": 100, "angle": -45
		index: 16,
		throttle: 0.5
	},
	{
		// "radius": 100, "angle": -45
		index: 17,
		throttle: 0.5
	},
	{
		// 4th switch
		// "length": 100, "switch": true
		index: 18,
		throttle: 0.5
	},
	{
		// "radius": 100, "angle": 45
		index: 19,
		throttle: 0.5
	},
	{
		//  "radius": 100, "angle": 45
		index: 20,
		throttle: 0.5
	},
	{
		//  "radius": 100, "angle": 45
		index: 21,
		throttle: 0.5
	},
	{
		//  "radius": 100, "angle": 45
		index: 22,
		throttle: 0.5
	},
	{
		// "radius": 200, "angle": 22.5
		index: 23,
		throttle: 0.5
	},
	{
		// "radius": 200, "angle": -22.5
		index: 24,
		throttle: 0.5
	},
	{
		// 5th switch
		// "length": 100, "switch": true
		index: 25,
		throttle: 0.5
	},
	{
		// "radius": 100, "angle": 45
		index: 26,
		throttle: 0.5
	},
	{
		// "radius": 100, "angle": 45
		index: 27,
		throttle: 0.4
	},
	{
		// "length": 62
		index: 28,
		throttle: 0.5
	},
	{
		// 6th switch
		// "radius": 100, "angle": -45, "switch": true
		index: 29,
		throttle: 0.5
	},
	{
		// "radius": 100, "angle": -45
		index: 30,
		throttle: 0.5
	},
	{
		// "radius": 100, "angle": 45
		index: 31,
		throttle: 0.1
	},
	{
		// "radius": 100, "angle": 45
		index: 32,
		throttle: 0.1
	},
	{
		// "radius": 100, "angle": 45
		index: 33,
		throttle: 0.1
	},
	{
		// "radius": 100, "angle": 45
		index: 34,
		throttle: 0.1
	},
	{
		 // 7th switch
		// "length": 100, "switch": true
		index: 35,
		throttle: 1.0
	},
	{
		//  "length": 100
		index: 36,
		throttle: 1.0
	},
	{
		//  "length": 100
		index: 37,
		throttle: 1.0
	},
	{
		//  "length": 100
		index: 38,
		throttle: 1.0
	},
	{
		//  "length": 100
		index: 39,
		throttle: 1.0
	}
]

module.exports.instructions = instructions;

