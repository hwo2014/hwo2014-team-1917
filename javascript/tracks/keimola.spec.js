var instructions = {};
// instructions.turnHard.throttle = 0.5;
// instructions.turnMedium.throttle = 0.7;
// instructions.turnEasy.throttle = 0.8;

instructions.pieces = [
	{
		// "length": 100
		index: 0,
		throttle: 1.0
	},
	{
		// "length": 100
		index: 1,
		throttle: 1.0
		,switchLane: "Right"
	},
	{
		// "length": 100
		index: 2,
		throttle: 0.5
	},
	{
		// 1st switch
		// "length": 100, "switch": true
		index: 3,
		throttle: 0.3
	},
	{
		// "radius": 100, "angle": 45
		index: 4,
		throttle: 0.3
	},
	{
		// "radius": 100, "angle": 45
		index: 5,
		throttle: 0.4
	},
	{
		// "radius": 100, "angle": 45
		index: 6,
		throttle: 0.5
	},
	{
		// "radius": 100, "angle": 45
		index: 7,
		throttle: 0.5
		,switchLane: "Left"
	}, 
	{	
		// 2nd switch
		// "radius": 200, "angle": 22.5, "switch": true
		index: 8,
		throttle: 0.7
	},
	{	
		// "length": 100
		index: 9,
		throttle: 1.0
	},
	{	
		// "length": 100
		index: 10,
		throttle: 1.0
	},
	{
		// "radius": 200, "angle": -22.5
		index: 11,
		throttle: 1.0
	},
	{
		// "length": 100
		index: 12,
		throttle: 0.3
	},
	{
		// 3rd switch
		// "length": 100, "switch": true
		index: 13,
		throttle: 0.5
		// ,switchLane: "Left"
	},
	{
		// "radius": 100, "angle": -45
		index: 14,
		throttle: 0.5
	},
	{
		//  "radius": 100, "angle": -45
		index: 15,
		throttle: 0.5
	},
	{
		// "radius": 100, "angle": -45
		index: 16,
		throttle: 0.5
	},
	{
		// "radius": 100, "angle": -45
		index: 17,
		throttle: 0.5
		,switchLane: "Right"
	},
	{
		// 4th switch
		// "length": 100, "switch": true
		index: 18,
		throttle: 0.7
	},
	{
		// "radius": 100, "angle": 45
		index: 19,
		throttle: 0.7
	},
	{
		//  "radius": 100, "angle": 45
		index: 20,
		throttle: 0.6
	},
	{
		//  "radius": 100, "angle": 45
		index: 21,
		throttle: 0.6
	},
	{
		//  "radius": 100, "angle": 45
		index: 22,
		throttle: 0.6
	},
	{
		// "radius": 200, "angle": 22.5
		index: 23,
		throttle: 0.6
	},
	{
		// "radius": 200, "angle": -22.5
		index: 24,
		throttle: 0.7
	},
	{
		// 5th switch
		// "length": 100, "switch": true
		index: 25,
		throttle: 0.6
	},
	{
		// "radius": 100, "angle": 45
		index: 26,
		throttle: 0.5
	},
	{
		// "radius": 100, "angle": 45
		index: 27,
		throttle: 0.4
	},
	{
		// "length": 62
		index: 28,
		throttle: 0.5
		,switchLane: "Left"
	},
	{
		// 6th switch
		// "radius": 100, "angle": -45, "switch": true
		index: 29,
		throttle: 0.6
	},
	{
		// "radius": 100, "angle": -45
		index: 30,
		throttle: 0.5
	},
	{
		// "radius": 100, "angle": 45
		index: 31,
		throttle: 0.6
	},
	{
		// "radius": 100, "angle": 45
		index: 32,
		throttle: 0.6
	},
	{
		// "radius": 100, "angle": 45
		index: 33,
		throttle: 0.6
	},
	{
		// "radius": 100, "angle": 45
		index: 34,
		throttle: 0.7
	},
	{
		 // 7th switch
		// "length": 100, "switch": true
		index: 35,
		throttle: 1.0
		// ,switchLane: "Left"
	},
	{
		//  "length": 100
		index: 36,
		throttle: 1.0
	},
	{
		//  "length": 100
		index: 37,
		throttle: 1.0
	},
	{
		//  "length": 100
		index: 38,
		throttle: 1.0
	},
	{
		//  "length": 100
		index: 39,
		throttle: 1.0
	}
]

module.exports.instructions = instructions;

