﻿var net = require("net");
var JSONStream = require('JSONStream');
var bot = require('./bot');
var fs = require('fs');
var path = require('path');
var log = require('./logWrapper');

var argv = require('yargs').argv;

var serverHost = argv._[0];
var serverPort = argv._[1];
var botName = argv._[2];
var botKey = argv._[3];

var trackName = argv.track || 'keimola';
var carCount = argv.carCount || 1;

log.info("start", {msg: "I'm "+ botName + " and connect to " + serverHost + ":" + serverPort});

if (argv.constantThrottle) {
    bot.setConstantThrottle(argv.constantThrottle);
}
var ignoreTurbo;
if (argv.turboOff) {
    ignoreTurbo = true;
}

var client = net.connect(serverPort, serverHost, function() {
    if (argv.track) { // create a custom race with our own bots
        return send({
            msgType: "joinRace",
            data: {
                botId: {
                    name: botName,
                    key: botKey
                },
                trackName: trackName,
                password: "LaakaPallo55",
                carCount: carCount
            }
        });
    } else if(argv.otherBots){
        // join a race with other people's bots!
        return send({
            msgType: "joinRace",
            data: {
                botId: {
                    name: botName,
                    key: botKey
                },
                carCount: carCount
            }
        });

    } else {
        // return simple join for the CI server
        return send({
            msgType: "join",
            data: {
                name: botName,
                key: botKey
            }
        });
    }
});

function send(json) {
    log.debug("sent msg", json);
    client.write(JSON.stringify(json));
    return client.write('\n');
}

var jsonStream = client.pipe(JSONStream.parse());

jsonStream.on('data', function(data) {
    try {
        log.debug("received msg", data);
        if (data.msgType === 'carPositions') {
            if (data.gameTick) { //only answer messages with gameTick
                var message = bot.getNextMessage(data.data, data.gameTick);
                send(message);
            }
        } else {
            if (data.msgType === 'join') {
                log.info('joined');
            } else if (data.msgType === 'gameStart') {
                log.info('Race started');
                bot.raceStarted();
                send({
                    msgType: "ping",
                    data: {}
                });
            } else if (data.msgType === 'gameEnd') {
                log.info('Race ended');
                bot.raceEnded(data.data);
                send({
                    msgType: "ping",
                    data: {}
                });
            } else if (data.msgType === 'gameInit') {
                log.info("initializing race");
                bot.initRace(data.data);
                // writeTrackSpecsToFile(data.data)
            } else if (data.msgType === 'yourCar') {
                log.info("initializing car");
                bot.initCar(data.data);
            } else if (data.msgType === 'crash') {
                log.info("car crashed: " + data.data.name);
                bot.crashed(data.data, data.gameTick);
            } else if (data.msgType === 'spawn') {
                log.info("car spawned: " + data.data.name);
                bot.spawned(data.data, data.gameTick);
            } else if (data.msgType === 'turboAvailable') {
                if (!ignoreTurbo) {
                    bot.turboAvailable(data.data);
                }
            } else if (data.msgType === 'turboStart') {
                bot.turboStarted(data.gameTick);
            } else if (data.msgType === 'turboEnd') {
                bot.turboEnded();
            } else if (data.msgType === 'lapFinished') {
                bot.lapFinished(data.data);
            } else if (data.msgType === 'dnf') {
                bot.dnf(data.data);
            } else if (data.msgType === 'finish') {
                bot.finished(data.data);
            } else if (data.msgType === 'error') {
                console.error("Error: %s", data.data);
            }

            /*send({
            msgType: "ping",
            data: {}
        });*/
        }
    } catch (ex) {
        if (ex.stack) {
            console.error("Error: ", ex.stack);
        } else {
            console.error("Error: ", ex);
        }

        console.log("Sending basic throttle");
        send({msgType:"throttle", data:0.5});
    }
});

jsonStream.on('error', function() {
    return log.error("disconnected");
});

// TODO: move this to away from here
function writeTrackSpecsToFile(data) {

    var trackname = data.race.track.name;
    var filepath = "tracks/" + trackname + ".json";

    fs.writeFile(path.join(__dirname, filepath), JSON.stringify(data), function(err) {
        if (err) {
            log.info("could not save track specs: " + err);
        } else {
            log.info("Track specs was saved to file!");
        }
    });
}
