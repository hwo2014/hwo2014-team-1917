var bot = require('../bot');
var switchLanesManager = require('../switchLanesManager');

var track = {
	pieces: [{
		"length": 100 // 0
	}, {
		"length": 100 // 1
	}, {
		"length": 100 // 2
	}, {
		"length": 100, // 3
		"switch": true // 1st switch
	}, {
		"radius": 100, // 4
		"angle": 45
	}, {
		"radius": 100, // 5
		"angle": 45
	}, {
		"radius": 100, // 6
		"angle": 45
	}, {
		"radius": 100, // 7
		"angle": 45
	}, {
		"radius": 200, // 8
		"angle": 22.5,
		"switch": true // 2nd switch
	}, {
		"length": 100 // 9
	}, {
		"length": 100 // 10
	}, {
		"radius": 200, // 11
		"angle": -22.5
	}, {
		"length": 100 // 12
	}, {
		"length": 100, // 13
		"switch": true // 3rd switch
	}, {
		"radius": 100, // 14
		"angle": -45
	}, {
		"radius": 100, // 15
		"angle": -45
	}, {
		"radius": 100, // 16
		"angle": -45
	}, {
		"radius": 100, // 17
		"angle": -45
	}, {
		"length": 100, // 18
		"switch": true // 4th switch
	}, {
		"radius": 100, // 19
		"angle": 45
	}, {
		"radius": 100, // 20
		"angle": 45
	}, {
		"radius": 100, // 21
		"angle": 45
	}, {
		"radius": 100, // 22
		"angle": 45
	}, {
		"radius": 200, // 23
		"angle": 22.5
	}, {
		"radius": 200, // 24
		"angle": -22.5
	}, {
		"length": 100, // 25
		"switch": true // 5th switch
	}, {
		"radius": 100, // 26
		"angle": 45
	}, {
		"radius": 100, // 27
		"angle": 45
	}, {
		"length": 62 // 28
	}, {
		"radius": 100, // 29
		"angle": -45,
		"switch": true // 6th switch
	}, {
		"radius": 100, // 30
		"angle": -45
	}, {
		"radius": 100, // 31
		"angle": 45
	}, {
		"radius": 100, // 32
		"angle": 45
	}, {
		"radius": 100, // 33
		"angle": 45
	}, {
		"radius": 100, // 34
		"angle": 45
	}, {
		"length": 100, // 35
		"switch": true // 7th switch
	}, {
		"length": 100 // 36
	}, {
		"length": 100 // 37
	}, {
		"length": 100 // 38
	}, {
		"length": 90 // 39
	}],
	lanes: [{
		"distanceFromCenter": -10,
		"index": 0
	}, {
		"distanceFromCenter": 10,
		"index": 1
	}]
}

describe("SwitchingLanesKeimola", function() {

	var pieces = track.pieces;
	var lanes = track.lanes;
	var mapPieceInfo = model.getMapPieceInfo(track.pieces, track.lanes);

	it("should request switch to right in first switch", function() {
		var nextSwitch = switchLanesManager.getNextLaneChange(mapPieceInfo, lanes, 0, 0);
		expect(nextSwitch.switchPending).toBe(true);
		expect(nextSwitch.direction).toBe("Right");
		expect(nextSwitch.pieceIndex).toBe(2);
	});

	it("should request switch to left in second switch", function() {
		var nextSwitch = switchLanesManager.getNextLaneChange(mapPieceInfo, lanes, 1, 6);
		expect(nextSwitch.switchPending).toBe(true);
		expect(nextSwitch.direction).toBe("Left");
		expect(nextSwitch.pieceIndex).toBe(7);
	});

	it("should request keeping the same lane in third switch", function() {
		var nextSwitch = switchLanesManager.getNextLaneChange(mapPieceInfo, lanes, 0, 10);
		expect(nextSwitch.switchPending).toBe(false);
	});

	it("should request switch to right in fourth switch", function() {
		var nextSwitch = switchLanesManager.getNextLaneChange(mapPieceInfo, lanes, 0, 14);
		expect(nextSwitch.switchPending).toBe(true);
		expect(nextSwitch.direction).toBe("Right");
		expect(nextSwitch.pieceIndex).toBe(17);
	});

	it("should request keeping the same lane in fifth switch", function() {
		var nextSwitch = switchLanesManager.getNextLaneChange(mapPieceInfo, lanes, 1, 20);
		expect(nextSwitch.switchPending).toBe(false);
	});

	it("should request keeping the same lane in sixth switch", function() {
		var nextSwitch = switchLanesManager.getNextLaneChange(mapPieceInfo, lanes, 1, 26);
		expect(nextSwitch.switchPending).toBe(false);
	});
	
	it("should request keeping the same lane in seventh switch", function() {
		var nextSwitch = switchLanesManager.getNextLaneChange(mapPieceInfo, lanes, 1, 30);
		expect(nextSwitch.switchPending).toBe(false);
	});
});