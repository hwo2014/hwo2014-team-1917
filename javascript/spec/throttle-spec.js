﻿var throttleManager = require('../throttleManager');
var model = require('../model');
var _ = require('underscore');
var model = require('../model');

describe("Throttle", function() {

    var m;
    var car = { name: "Schumacher", color: "red" };

    beforeEach(function() {
        m = model.initModel();
        m.initRace({ race: { "track": { "id": "keimola", "name": "Keimola", "pieces": [{ "length": 100 }, { "length": 100 }, { "length": 100 }, { "length": 100, "switch": true }, { "radius": 100, "angle": 45 }, { "radius": 100, "angle": 45 }, { "radius": 100, "angle": 45 }, { "radius": 100, "angle": 45 }, { "radius": 200, "angle": 22.5, "switch": true }, { "length": 100 }, { "length": 100 }, { "radius": 200, "angle": -22.5 }, { "length": 100 }, { "length": 100, "switch": true }, { "radius": 100, "angle": -45 }, { "radius": 100, "angle": -45 }, { "radius": 100, "angle": -45 }, { "radius": 100, "angle": -45 }, { "length": 100, "switch": true }, { "radius": 100, "angle": 45 }, { "radius": 100, "angle": 45 }, { "radius": 100, "angle": 45 }, { "radius": 100, "angle": 45 }, { "radius": 200, "angle": 22.5 }, { "radius": 200, "angle": -22.5 }, { "length": 100, "switch": true }, { "radius": 100, "angle": 45 }, { "radius": 100, "angle": 45 }, { "length": 62 }, { "radius": 100, "angle": -45, "switch": true }, { "radius": 100, "angle": -45 }, { "radius": 100, "angle": 45 }, { "radius": 100, "angle": 45 }, { "radius": 100, "angle": 45 }, { "radius": 100, "angle": 45 }, { "length": 100, "switch": true }, { "length": 100 }, { "length": 100 }, { "length": 100 }, { "length": 90 }], "lanes": [{ "distanceFromCenter": -10, "index": 0 }, { "distanceFromCenter": 10, "index": 1 }], "startingPoint": { "position": { "x": -300, "y": -44 }, "angle": 90 } }, "cars": [{ "id": { "name": "janne", "color": "red" }, "dimensions": { "length": 40, "width": 20, "guideFlagPosition": 10 } }], "raceSession": { "laps": 3, "maxLapTimeMs": 60000, "quickRace": true } } });
        m.initCar(car);
    });

    var updateStatus = function(position, velocity, throttle) {
        var v = velocity || 6.0;
        var t = throttle || 1.0;
        
        var prevPos = _.extend(position, { inPieceDistance: position.inPieceDistance - v });

        m.updateStatus([{ "id": car, "angle": 0.0, "piecePosition": prevPos }], 124);

        m.myStatus.throttle = t;
        m.updateStatus([{ "id": car, "angle": 0.0, "piecePosition": position }], 125);
    };

    xit("should return throttle off if we need to start slowing down for the corner", function() {
        updateStatus({
            pieceIndex: 3,
            inPieceDistance: 55.6,
            lane: { startLaneIndex: 0, endLaneIndex: 0 }
        }, 8.5);

        var throttle = throttleManager.calculateThrottle(m);

        expect(throttle.throttle).toBe(0);
    });

    it("should return throttle on if there is no need to slow down", function() {
        updateStatus({
            pieceIndex: 2,
            inPieceDistance: 15.6,
            lane: { startLaneIndex: 0, endLaneIndex: 0 }
        }, 6.3);

        var throttle = throttleManager.calculateThrottle(m);

        expect(throttle.throttle).toBe(1);
    });

    it("should return throttle on if there is no need to slow down in the corner", function() {
        updateStatus({
            pieceIndex: 5,
            inPieceDistance: 15.6,
            lane: { startLaneIndex: 0, endLaneIndex: 0 }
        }, 7.6, 0.3);

        var throttle = throttleManager.calculateThrottle(m);

        expect(throttle.throttle).toBe(1);
    });

    xit("should return throttle off if there is need to slow down for the inside track", function() {
        updateStatus({
            pieceIndex: 16,
            inPieceDistance: 15.6,
            lane: { startLaneIndex: 0, endLaneIndex: 0 }
        }, 6.8); // just above the limit for inside track, but under the limit for outside track);

        var throttle = throttleManager.calculateThrottle(m);

        expect(throttle.throttle).toBe(0);
    });
});