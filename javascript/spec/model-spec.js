﻿var _ = require('underscore');

var model = require('../model');

describe("Cornering coefficient calculation", function() {

    var m;
    beforeEach(function() {
        m = model.initModel();
        m.initRace({ race: { "track": { "id": "keimola", "name": "Keimola", "pieces": [{ "length": 100 }, { "length": 100 }, { "length": 100 }, { "length": 100, "switch": true }, { "radius": 100, "angle": 45 }, { "radius": 100, "angle": 45 }, { "radius": 100, "angle": 45 }, { "radius": 100, "angle": 45 }, { "radius": 200, "angle": 22.5, "switch": true }, { "length": 100 }, { "length": 100 }, { "radius": 200, "angle": -22.5 }, { "length": 100 }, { "length": 100, "switch": true }, { "radius": 100, "angle": -45 }, { "radius": 100, "angle": -45 }, { "radius": 100, "angle": -45 }, { "radius": 100, "angle": -45 }, { "length": 100, "switch": true }, { "radius": 100, "angle": 45 }, { "radius": 100, "angle": 45 }, { "radius": 100, "angle": 45 }, { "radius": 100, "angle": 45 }, { "radius": 200, "angle": 22.5 }, { "radius": 200, "angle": -22.5 }, { "length": 100, "switch": true }, { "radius": 100, "angle": 45 }, { "radius": 100, "angle": 45 }, { "length": 62 }, { "radius": 100, "angle": -45, "switch": true }, { "radius": 100, "angle": -45 }, { "radius": 100, "angle": 45 }, { "radius": 100, "angle": 45 }, { "radius": 100, "angle": 45 }, { "radius": 100, "angle": 45 }, { "length": 100, "switch": true }, { "length": 100 }, { "length": 100 }, { "length": 100 }, { "length": 90 }], "lanes": [{ "distanceFromCenter": -10, "index": 0 }, { "distanceFromCenter": 10, "index": 1 }], "startingPoint": { "position": { "x": -300, "y": -44 }, "angle": 90 } }, "cars": [{ "id": { "name": "janne", "color": "red" }, "dimensions": { "length": 40, "width": 20, "guideFlagPosition": 10 } }], "raceSession": { "laps": 3, "maxLapTimeMs": 60000, "quickRace": true } } });
        m.initCar({ name: "schumi", color: "red" });
    });

    it("should up the max speed for next corner if last corner did not drift enough", function() {
        var originalCoefficient = m.environmentVariables.corneringSpeedCoefficient;

        m.updateCornerStatistics({ pieceIndex: 1, inPiecePosition: 12.2, lane: {endLaneIndex: 0} }, 5.2, 0, 0);
        m.updateCornerStatistics({ pieceIndex: 4, inPiecePosition: 12.2, lane: {endLaneIndex: 0} }, 7.9, 10, 0);
        m.updateCornerStatistics({ pieceIndex: 4, inPiecePosition: 32.2, lane: {endLaneIndex: 0} }, 8.0, 10, 0);
        m.updateCornerStatistics({ pieceIndex: 5, inPiecePosition: 52.2, lane: {endLaneIndex: 0} }, 7.9, 12, 0);

        m.updateCornerStatistics({ pieceIndex: 9 }); // after the turn

        expect(m.environmentVariables.corneringSpeedCoefficient).toBeGreaterThan(originalCoefficient);
    });

    it("should not up the max speed for next corner if last corner had enough drifting", function() {
        var originalCoefficient = m.environmentVariables.corneringSpeedCoefficient;

        m.updateCornerStatistics({ pieceIndex: 4, inPiecePosition: 12.2, lane: {endLaneIndex: 0} }, 7.9, -20, 0);
        m.updateCornerStatistics({ pieceIndex: 4, inPiecePosition: 32.2, lane: {endLaneIndex: 0} }, 8.0, -30, 0);
        m.updateCornerStatistics({ pieceIndex: 5, inPiecePosition: 52.2, lane: {endLaneIndex: 0}}, 7.9, -41, 0);

        m.updateCornerStatistics({ pieceIndex: 9 }); // after the turn

        expect(m.environmentVariables.corneringSpeedCoefficient).toBe(originalCoefficient);
    });

    it("should not up the max speed for next corner if previously there was a crash", function() {        
        m.updateCornerStatistics({ pieceIndex: 4, inPiecePosition: 12.2, lane: {endLaneIndex: 0} }, 7.9, -40, 0);
        m.reportCrash();

        var maxCoefficient = m.environmentVariables.corneringSpeedCoefficient;

        m.updateCornerStatistics({ pieceIndex: 4, inPiecePosition: 32.2, lane: {endLaneIndex: 0} }, 8.0, 30, 0);
        m.updateCornerStatistics({ pieceIndex: 5, inPiecePosition: 52.2, lane: {endLaneIndex: 0} }, 7.9, -41, 0);

        m.updateCornerStatistics({ pieceIndex: 9 }); // after the turn

        expect(m.environmentVariables.corneringSpeedCoefficient).toBeLessThan(maxCoefficient);
    });
});

describe("Speed calculation", function() {

    var roundToTwoDigits = function(n) {
        return Math.round(n * 100) / 100;
    };

    var m;
    beforeEach(function() {
        m = model.initModel();
        m.initRace({ race: { "track": { "id": "keimola", "name": "Keimola", "pieces": [{ "length": 100 }, { "length": 100 }, { "length": 100 }, { "length": 100, "switch": true }, { "radius": 100, "angle": 45 }, { "radius": 100, "angle": 45 }, { "radius": 100, "angle": 45 }, { "radius": 100, "angle": 45 }, { "radius": 200, "angle": 22.5, "switch": true }, { "length": 100 }, { "length": 100 }, { "radius": 200, "angle": -22.5 }, { "length": 100 }, { "length": 100, "switch": true }, { "radius": 100, "angle": -45 }, { "radius": 100, "angle": -45 }, { "radius": 100, "angle": -45 }, { "radius": 100, "angle": -45 }, { "length": 100, "switch": true }, { "radius": 100, "angle": 45 }, { "radius": 100, "angle": 45 }, { "radius": 100, "angle": 45 }, { "radius": 100, "angle": 45 }, { "radius": 200, "angle": 22.5 }, { "radius": 200, "angle": -22.5 }, { "length": 100, "switch": true }, { "radius": 100, "angle": 45 }, { "radius": 100, "angle": 45 }, { "length": 62 }, { "radius": 100, "angle": -45, "switch": true }, { "radius": 100, "angle": -45 }, { "radius": 100, "angle": 45 }, { "radius": 100, "angle": 45 }, { "radius": 100, "angle": 45 }, { "radius": 100, "angle": 45 }, { "length": 100, "switch": true }, { "length": 100 }, { "length": 100 }, { "length": 100 }, { "length": 90 }], "lanes": [{ "distanceFromCenter": -10, "index": 0 }, { "distanceFromCenter": 10, "index": 1 }], "startingPoint": { "position": { "x": -300, "y": -44 }, "angle": 90 } }, "cars": [{ "id": { "name": "janne", "color": "red" }, "dimensions": { "length": 40, "width": 20, "guideFlagPosition": 10 } }], "raceSession": { "laps": 3, "maxLapTimeMs": 60000, "quickRace": true } } });
        m.initCar({ name: "Schumacher", color: "red" });
    });

    it("should return correct speed for in-piece distance", function() {
        m.updateStatus([
            {
                "id": {
                    "name": "Schumacher",
                    "color": "red"
                },
                "angle": 0.0,
                "piecePosition": {
                    "pieceIndex": 0,
                    "inPieceDistance": 2.0,
                    "lane": {
                        "startLaneIndex": 0,
                        "endLaneIndex": 0
                    },
                    "lap": 0
                }
            }
        ], 124);

        m.updateStatus([
            {
                "id": {
                    "name": "Schumacher",
                    "color": "red"
                },
                "angle": 0.0,
                "piecePosition": {
                    "pieceIndex": 0,
                    "inPieceDistance": 5.3,
                    "lane": {
                        "startLaneIndex": 0,
                        "endLaneIndex": 0
                    },
                    "lap": 0
                }
            }
        ], 125);

        expect((Math.round(m.myStatus.velocity * 100) / 100)).toBe(3.3);       
    });

     it("should return correct speed for distance between pieces", function() {
         m.updateStatus([
             {
                 "id": {
                     "name": "Schumacher",
                     "color": "red"
                 },
                 "angle": 0.0,
                 "piecePosition": {
                     "pieceIndex": 0,
                     "inPieceDistance": 97.2,
                     "lane": {
                         "startLaneIndex": 0,
                         "endLaneIndex": 0
                     },
                     "lap": 0
                 }
             }
         ], 124);

         m.updateStatus([
             {
                "id": {
                    "name": "Schumacher",
                    "color": "red"
                },
                "angle": 0.0,
                "piecePosition": {
                    "pieceIndex": 1,
                    "inPieceDistance": 2.3,
                    "lane": {
                        "startLaneIndex": 0,
                        "endLaneIndex": 0
                    },
                    "lap": 0
                }
            }
        ], 124);

        expect(Math.round(m.myStatus.velocity * 100) / 100).toBe(2.8 + 2.3);
    });

    it("should return correct speed for distance between pieces", function() {
        m.updateStatus([
            {
                "id": {
                    "name": "Schumacher",
                    "color": "red"
                },
                "angle": 0.0,
                "piecePosition": {
                    "pieceIndex": 0,
                    "inPieceDistance": 97.2,
                    "lane": {
                        "startLaneIndex": 0,
                        "endLaneIndex": 0
                    },
                    "lap": 0
                }
            }
        ], 124);

        m.updateStatus([
            {
                "id": {
                    "name": "Schumacher",
                    "color": "red"
                },
                "angle": 0.0,
                "piecePosition": {
                    "pieceIndex": 1,
                    "inPieceDistance": 2.3,
                    "lane": {
                        "startLaneIndex": 0,
                        "endLaneIndex": 0
                    },
                    "lap": 0
                }
            }
        ], 124);

        expect(Math.round(m.myStatus.velocity * 100) / 100).toBe(2.8 + 2.3);
    });

    it("should return correct speed for distance after switch lanes", function() {
        m.updateStatus([
            {
                "id": {
                    "name": "Schumacher",
                    "color": "red"
                },
                "angle": 0.0,
                "piecePosition": {
                    "pieceIndex": 18,
                    "inPieceDistance": 97.2,
                    "lane": {
                        "startLaneIndex": 0,
                        "endLaneIndex": 1
                    },
                    "lap": 0
                }
            }
        ], 124);

        m.updateStatus([
            {
                "id": {
                    "name": "Schumacher",
                    "color": "red"
                },
                "angle": 0.0,
                "piecePosition": {
                    "pieceIndex": 19,
                    "inPieceDistance": 2.3,
                    "lane": {
                        "startLaneIndex": 1,
                        "endLaneIndex": 1
                    },
                    "lap": 0
                }
            }
        ], 125);

        var diagonalLaneLength = Math.sqrt(Math.pow(100, 2) + Math.pow(20, 2));
        expect(roundToTwoDigits(m.myStatus.velocity)).toBe(roundToTwoDigits(diagonalLaneLength - 97.2 + 2.3));
    });

    it("should return correct distance to next curve", function() {
        m.updateStatus([
            {
                "id": {
                    "name": "Schumacher",
                    "color": "red"
                },
                "angle": 0.0,
                "piecePosition": {
                    "pieceIndex": 12,
                    "inPieceDistance": 97.2,
                    "lane": {
                        "startLaneIndex": 0,
                        "endLaneIndex": 1
                    },
                    "lap": 0
                }
            }
        ], 124);

        var nextCurve = m.getNextCurveInfo();

        expect(nextCurve.distance).toBeLessThan(150);

    });
});


