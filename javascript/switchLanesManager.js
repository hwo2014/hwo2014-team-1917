var _ = require("underscore");
var log = require("./logWrapper");

var getNextSwitch = function(mapPieceInfo, pieceIndex) {

    var nextSwitch = _.find(mapPieceInfo, function(piece) {
        return piece.hasSwitch && piece.pieceIndex > pieceIndex;
    });

	return nextSwitch;
}

// check what is the shortest route from next swith to switch after that
// TODO: we need to know which way we are driving in order to give correct directions (left vs right)
exports.getNextLaneChange = function(mapPieceInfo, lanes, currentLane, currentPiece) {

	// result could be info about the latest piece where swith message should be sent
	// and direction of switch
	var switchPending = false;
	var pieceIndex = -1;
	var direction = "";

	// get next possible switch place
	// TODO: do we need to sort or can we count on the order of the pieces?
	var nextSwitch = getNextSwitch(mapPieceInfo, currentPiece);

	// TODO: currently checking switches only in single lap, not checking next switch from next lap
	// TODO: also need to add some tolerance to length difference
	if (!_.isUndefined(nextSwitch)) {

		// TODO: nextSwitch.pieceIndex might be null
		var followingSwitch = getNextSwitch(mapPieceInfo, nextSwitch.pieceIndex);

		if (!_.isUndefined(followingSwitch)) {

			// get all pieces between next two switches
			var pieces = _.filter(mapPieceInfo, function(p) {
				return p.pieceIndex > nextSwitch.pieceIndex && p.pieceIndex < followingSwitch.pieceIndex;
			});

			var lanesWithLengths = [];

			// loop each piece as many times as there are lanes to get individual lengths
			for (var i = 0; i < lanes.length; i++) {

				var lane = lanes[i];
				var length = 0;

				_.each(pieces, function(piece) {
					length = length + piece.laneLengths[i];
				});

				lanesWithLengths.push({
					lane: lane.index,
					totalLength: length
				});
			};

			var shortestLane = _.min(lanesWithLengths, function(lane) {
				return lane.totalLength;
			}).lane;

			log.debug("checking shortest lane between switches in index " + nextSwitch.pieceIndex + " and " + followingSwitch.pieceIndex);
			log.debug("shortest lane: " + shortestLane);

			// if we are not in the shortest track then we need to change lanes
			if (currentLane !== shortestLane) {
				switchPending = true;
				if (shortestLane > currentLane) {
					direction = "Right";
				} else {
					direction = "Left";
				}
			};

			pieceIndex = nextSwitch.pieceIndex - 1;
		}
	};

	var result = {
		switchPending: switchPending, // if we are alredy in optimal track and we dont need to change lanes
		pieceIndex: pieceIndex, // latest pieceIndex were we need to call switch
		direction: direction, // which way to switch
	};

	return result;
}