﻿var net = require( "net" );
var JSONStream = require( 'JSONStream' );
var bot = require( './bot' );

var serverHost = process.argv[2];
var serverPort = process.argv[3];
var botName = process.argv[4];
var botKey = process.argv[5];

var track = "Keimola";
var password = "LaakaPallo55";
var carCount = 2;

console.log("I'm", botName, "and connect to", serverHost + ":" + serverPort);

console.log("Creating new track and waiting others to join...")
client = net.connect(serverPort, serverHost, function () {
    return send({
        msgType: "createRace",
        data: {
            botId: {
                name: botName,
                key: botKey
            },
            trackname: track,
            password: password,
            carCount: carCount
        }
    });
});

function send(json) {
    client.write(JSON.stringify(json) );
    return client.write('\n');
};

jsonStream = client.pipe(JSONStream.parse());

jsonStream.on('data', function (data) {
    console.log( "%j", data);
    if (data.msgType === 'carPositions') {

        send( {
            msgType: "throttle",
            data: bot.updateStatusAndCalculateThrottle( data.data, data.gametick )
        });
        /*
            // uncomment for testing with keimola 'baseline'
            var msg = bot.getFixedInstructionsForKeimola(data.data, data.gametick);
            send({
                msgType: msg.msgType,
                data: msg.data
            });
        */
    } else {
        if ( data.msgType === 'join' ) {
            console.log( 'joined' );
        } else if ( data.msgType === 'gameStart' ) {
            console.log( 'Race started' );
        } else if ( data.msgType === 'gameEnd' ) {
            console.log( 'Race ended' );
        } else if ( data.msgType === 'gameInit' ) {
            console.log( "initializing race" );
            bot.initRace(data.data);
        } else if ( data.msgType === 'yourCar' ) {
            console.log( "initializing car" );
            bot.initCar(data.data);
        } else if (data.msgType === 'crash') {
            console.log( "car crashed: " + data.data.name);
        };

        send({
            msgType: "ping",
            data: {}
        });
    }
});

jsonStream.on('error', function () {
    return console.log("disconnected");
});

