﻿var cpe = require("./carStatusEstimate");

var jog = require("./logWrapper");

var ThrottleCalculator = (function () {
    function ThrottleCalculator(mapPieces, environmentVariables) {
        this.mapPieces = mapPieces;
        this.environmentVariables = environmentVariables;
    }
    ThrottleCalculator.prototype.getMaxSafeThrottle = function (currentThrottle, position, currentVelocity, currentSlipAngle, currentSlipAngleVelocity, turbo) {
        for (var t = 1.0; t > 0; t = t - 0.01) {
            t = Math.round(t * 100) / 100;
            var estimate = new cpe.CarStatusEstimate(this.mapPieces, position, currentVelocity, currentSlipAngle, currentSlipAngleVelocity, turbo && turbo.stillOnForTicks, turbo && turbo.data.turboFactor, this.environmentVariables);
            estimate.updateOneStep(currentThrottle);
            var crash = this.wouldCarCrashWithThrottle(t, estimate, turbo !== undefined && turbo !== null);
            if (!crash) {
                jog.debug("throttle", { msg: "car would not crash with throttle " + t, angles: estimate.slipAngles });
                return t;
            }
        }

        // no safe throttle, kill it!
        return 0.0;
    };

    ThrottleCalculator.prototype.wouldCarCrashWithThrottle = function (throttle, estimate, turbo) {
        for (var step = 0; step < (turbo ? 40 : 20); step++) {
            estimate.updateOneStep(throttle);
            if (Math.abs(estimate.currentSlipAngle) > 59.5) {
                // oh noes, the car would crash!
                jog.debug("throttle", { msg: "car would crash with throttle " + throttle + " on step+" + step + " at " + estimate.position.lap + ":" + estimate.position.pieceIndex + ":" + estimate.position.inPieceDistance, throttle: throttle, angle: estimate.currentSlipAngle, velocity: estimate.currentVelocity });
                return true;
            }
        }

        // if the estimate would end in a corner, check it to the end
        var estimatedPieceAngle = estimate.currentPieceAngle;
        if (Math.abs(estimate.currentPieceAngle) > 0) {
            var nextAngle = estimatedPieceAngle;
            while (nextAngle === estimatedPieceAngle) {
                step++;
                estimate.updateOneStep(throttle);
                if (Math.abs(estimate.currentSlipAngle) > 60) {
                    // oh noes, the car would crash!
                    jog.debug("throttle", { msg: "car would crash with throttle " + throttle + " on corner step+" + step + " at " + estimate.position.lap + ":" + estimate.position.pieceIndex + ":" + estimate.position.inPieceDistance, throttle: throttle, angle: estimate.currentSlipAngle, velocity: estimate.currentVelocity });
                    return true;
                }
                nextAngle = estimate.currentPieceAngle;
            }
        }
        return false;
    };
    return ThrottleCalculator;
})();
exports.ThrottleCalculator = ThrottleCalculator;
//# sourceMappingURL=throttleCalculator.js.map
