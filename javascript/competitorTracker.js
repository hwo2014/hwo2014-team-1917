var _ = require('underscore');
var jog = require('./logWrapper');

var competitors = [];
var ownData = {};
var others = [];

var initializeLocations = function(myName, positions){
	var sortedPositions = _.sortBy(positions, function(el){
		return el.inPieceDistance;
	});

	var me = _.find(positions, function(el){ return el.id.name === myName;});

	var i = 0;
	competitors = _.map(sortedPositions, function(el){
		var competitor = {
			name: el.id.name,
			isSelf: el.id.name === myName,
			driver: el.id.name,
			color: el.id.color,
			position: i++,
			lap: el.lap,
			startLane: el.piecePosition.lane.startLaneIndex,
			endLane: el.piecePosition.lane.endLaneIndex,
			angle: el.angle,
			distanceFromSelf: me.piecePosition.inPieceDistance - el.piecePosition.inPieceDistance,
			speed: 0,
			acceleration: 0,
			isCrashed: false,
			inPieceDistance: el.piecePosition.inPieceDistance,
			pieceIndex: el.piecePosition.pieceIndex,
			lapTimes: [],
			finished: false,
			dnf: false
		};
		if(competitor.isSelf){
			ownData = competitor;
		} else {
			others.push(competitor);
		}
		return competitor;
	});
};

var calculateSpeed = function(competitor, newData, mapPieceInfo){
	var distance = 0;

	var pieceIndex = competitor.pieceIndex;
	if (pieceIndex != newData.piecePosition.pieceIndex){
		while(pieceIndex != newData.piecePosition.pieceIndex){
			if(competitor.pieceIndex == pieceIndex){
				distance += mapPieceInfo[pieceIndex].laneLengths[competitor.endLaneIndex] - competitor.inPieceDistance;
			} else {
				distance += mapPieceInfo[pieceIndex].laneLengths[competitor.endLaneIndex];
			}
			pieceIndex = (pieceIndex + 1) % mapPieceInfo.length;
		}
		distance += newData.piecePosition.inPieceDistance;
	} else {
		distance += newData.piecePosition.inPieceDistance - competitor.inPieceDistance
	}
	
	return distance; //distance traveled is speed
}

var calculateDistanceFromSelf = function(competitor, mapPieceInfo){
	if(competitor.isSelf){
		return 0; // no need to calculate distance from self
	}

	var iAmInLead = ownData.lap > competitor.lap || (ownData.lap == competitor.lap && (ownData.pieceIndex > competitor.pieceIndex || (ownData.pieceIndex == competitor.pieceIndex && ownData.inPieceDistance > competitor.inPieceDistance)));
	
	var pursuer = iAmInLead ? competitor : ownData;
	var leader = iAmInLead ? ownData : competitor;

	var distance = 0;
	var lap = pursuer.lap;
	var pieceIndex = pursuer.pieceIndex;

	//jog.debug("competitor", {iAmInLead: iAmInLead, pursuer: pursuer, leader: leader});

	if (lap < leader.lap || pieceIndex != leader.pieceIndex){
		distance += mapPieceInfo[pieceIndex].laneLengths[pursuer.endLane] - pursuer.inPieceDistance;
		pieceIndex = (pieceIndex + 1) % mapPieceInfo.length
		while(lap < leader.lap || pieceIndex != leader.pieceIndex){
			distance += mapPieceInfo[pieceIndex].laneLengths[pursuer.endLane];
			pieceIndex = (pieceIndex + 1) % mapPieceInfo.length;
		}
		distance += leader.inPieceDistance;
	} else {
		distance += leader.inPieceDistance - pursuer.inPieceDistance; // both on the same lap and piece
	}
	return iAmInLead ? -distance : distance; //distance is negative if we are in the lead
};

var updateLocations = function(myName, positions, mapPieceInfo){
	var updateDataForCompetitor = function(competitor){
		var newData = _.find(positions, function(data) {return data.id.name == competitor.name;});
		var newSpeed = calculateSpeed(competitor, newData, mapPieceInfo);
		competitor.acceleration = newSpeed - competitor.speed;
		competitor.speed = newSpeed;
		competitor.lap = newData.lap;
		competitor.startLane = newData.piecePosition.startLaneIndex;
		competitor.endLane = newData.piecePosition.endLaneIndex;
		if (competitor.isCrashed && competitor.speed > 0){
			competitor.isCrashed = false;
		}
		competitor.inPieceDistance = newData.piecePosition.inPieceDistance;
		competitor.pieceIndex = newData.piecePosition.pieceIndex;

		competitor.distanceFromSelf = calculateDistanceFromSelf(competitor, mapPieceInfo);
	}

	updateDataForCompetitor(ownData); // update own data first so the distance calculations work

	_.each(others, updateDataForCompetitor);
};

var updatePositions = function(){
	_.each(competitors, function(competitor) {
		competitor.position = _.sortedIndex(competitors, competitor, function(el) {
			return el.lap * 100000 + el.pieceIndex * 10000 + el.inPieceDistance;
			});
	});
}

exports.competitors = competitors;

exports.updateData = function(myName, positions, mapPieceInfo){

	if(competitors.length == 0){

		initializeLocations(myName, positions);
	} else {
		updateLocations(myName, positions, mapPieceInfo);
	}

	updatePositions();
	jog.debug("competitors", {competitors: competitors});
	return competitors;
};

exports.crashed = function(crashData){
	var crasher = _.findWhere(competitors, {'name': crashData.name});
	crasher.isCrashed = true;	
};

exports.finished = function(data){
	_.findWhere(competitors, {name: data.name}).finished = true;
};

exports.lapFinished = function(data){
	var competitor = _.findWhere(competitors, {name: data.car.name});
	competitor.lapTimes.push({
		lap: data.lapTime.lap,
		ticks: data.lapTime.ticks,
		millis: data.lapTime.millis
	});
};

exports.dnf = function(name) {
    var competitor = _.findWhere(competitors, { name: name });
    if (competitor) {
        competitor.dnf = true;
    } else {
        jog.warn("competitors", { msg: "could not report DNF for competitor " + name });
    }
};
