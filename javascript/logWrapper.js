﻿var jog = require('jog');
var dateformat = require('dateformat');
var argv = require('yargs').argv;

var timestamp = dateformat(Date().now, "yyyymmdd_HHMMss");
var track = argv.track || 'keimola';
var botName = argv._[2];

var log = jog(new jog.FileStore('logs/' + timestamp + "-" + track + "-" + botName + ".jog"));

module.exports = log;
