var _ = require( 'underscore' );
var log = require( './logWrapper' );

var speedData = [];
var speedDataForRollResistance = [];
var speedDataForDrag = [];
var rollResistanceDataTickCounter = 0;

var model = {
    dragCoefficient: 0,//,
    rollResistance: 0.02401,
    engineOutput: 0, //F(eng)
    maxSpeed: 0
}

var updateRollResistance = function () {
    if ( speedDataForRollResistance.length <= 3 ) {
        return; // not enough data yet
    }
    //// roll resistance data should be with low speeds -> drag should be minimal: F(long) = F(eng) + F(rr)
    //var totalWeight = 0;
    var coefficients = _.map( speedDataForRollResistance, function ( data ) {
        //var weight = model.maxSpeed / data.speed;
        //log.debug('weight is ' + weight);
        //totalWeight += weight; // assign more weight on low speeds

        var rollResistance = 0;

        var acceleration = Math.abs( data.acceleration );
        //log.debug('acceleration is ' + acceleration);


        //log.debug('engine output is ' + model.engineOutput);
        //log.debug( "calculating roll resistance: (" + model.engineOutput + " * " + data.throttle + " - " + acceleration + ") / " + data.speed );
        rollResistance = ( ( model.engineOutput * data.throttle - acceleration ) / data.speed ); //weight;


        //log.debug( "roll resistance is " + rollResistance + " for %j", data );

        return rollResistance;
    });

    //model.rollResistance = _.reduce( coefficients, function ( memo, num ) {
    //    return memo + num;
    //}, 0 ) / speedDataForRollResistance.length; // average

    //model.rollResistance = coefficients[Math.floor(coefficients.length / 2)];
    model.rollResistance = _.max(coefficients);
};

// drag calculation is not being used for now
var updateDrag = function () {
    var totalWeight = 0;
    var coefficients = _.map( speedDataForDrag, function ( data ) {
        var weight = data.speed; // assign more weight on higher speeds
        totalWeight += weight;
        var acceleration = Math.abs( data.acceleration );
        var drag = 0;

        if ( data.throttle == 0 ) { // no forward thrust, only roll resistance and drag
            drag = ( acceleration - data.speed * model.rollResistance ) / ( data.speed * data.speed );
        } else {
            drag = ( ( acceleration / data.throttle ) - model.engineOutput + data.speed * model.rollResistance ) / ( data.speed * data.speed ) * weight;
        }

        //log.debug( "drag is " + drag + " for %j", data );

        return drag;
    });

    model.dragCoefficient = _.reduce( coefficients, function ( memo, num ) {
        return memo + num;
    }, 0 ) / totalWeight;
}
var updateCoefficients = function ( latestData ) {

    if ( latestData.tick == 1 ) {
        // add a few next ticks to roll resistance calculation list
        rollResistanceDataTickCounter = 50;
    }

    if ( latestData.speed == 0 ) {
        // add a few next ticks to roll resistance calculation list
        rollResistanceDataTickCounter = 50;
        return; // ignore ticks where the speed is 0 -> there is no roll resistance or drag
    }

    // update data:

    if ( rollResistanceDataTickCounter-- > 0 ) {
        speedDataForRollResistance.push( latestData );
        updateRollResistance();
    }


    return; // skip processing for drag for now 

    // we're braking and near max speed
    if ( latestData.throttle == 0 && latestData.speed >= 0.9 * model.maxSpeed ) {
        speedDataForDrag.push( latestData );
        updateDrag();
    }
};

var addSpeedData = function ( tick, speed, acceleration, throttle, turbo) {
    var latestSpeedData = {
        tick: tick,
        speed: speed,
        acceleration: acceleration,
        throttle: throttle
    };
    
    speedData.splice(_.sortedIndex(speedData, latestSpeedData, 'speed'), 0, latestSpeedData );

    if ( speed > model.maxSpeed ) {
        model.maxSpeed = speed; // update max observed speed
    }
    
    if ( throttle == 0 && acceleration > 0 ) {
        log.error( 'Acceleration was positive, but throttle was 0. Expecting negative acceleration value when car is slowing down. Has acceleration been inverted again?' );
    }

    if ( acceleration > 0 && throttle > 0 ) { // Accelerating
        var turboBoost = turbo ? turbo.data.turboFactor : 1.0;
        var engineOutput = turboBoost * Math.abs( acceleration ) / throttle;
        log.debug("physics", {
            acceleration: acceleration,
            throttle: throttle,
            turboFactor: turboBoost,
            newEngineOutput: engineOutput
        });

        if ( model.engineOutput < engineOutput ) { // engine output is the maximum output the engine can produce
            model.engineOutput = engineOutput;
        }

        //TODO: should we return the engine output back to normal?
    }

    updateCoefficients( latestSpeedData );
};



exports.addSpeedData = addSpeedData;
exports.model = model;
