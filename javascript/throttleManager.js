﻿var _ = require("underscore");
var jog = require("./logWrapper");

exports.calculateThrottle = function(model) {
    var result = {
        throttle: 0.0
    };
    var getAccelerationForThrottle = function(throttle, currentSpeed, engineOutput, turboFactor, rollingResistance) {
        if (!turboFactor) {
            turboFactor = 1;
        }
        //return (engineOutput * throttle * turboFactor) / Math.exp((1 + throttle)) - rollingResistance * currentSpeed;
        return (engineOutput * throttle * turboFactor) / 5 - rollingResistance * currentSpeed;
    }

    var getThrottleForTargetSpeed = function(targetSpeed, currentSpeed, currentThrottle, turboFactor, engineOutput, rollingResistance) {
        if (!targetSpeed) {
            throw "target speed cannot be falsy";
        } 
        if (!currentSpeed && currentSpeed !== 0) {
            throw "current speed cannot be empty";
        }
        var throttle = currentThrottle || 0; // this can be left undefined right after spawning
        
        var accelerationForCurrentTick = getAccelerationForThrottle(throttle, currentSpeed, turboFactor, engineOutput, rollingResistance);
        var speedAfterCurrentTick = currentSpeed + accelerationForCurrentTick;

        var requiredAcceleration = targetSpeed - currentSpeed;

        if (requiredAcceleration > 0.2) {
            return 1.0;        
        } else {
            var resultAcceleration = 0;
            if (requiredAcceleration < 0) {
                var accelerationForZeroThrottle = getAccelerationForThrottle(0.0, speedAfterCurrentTick, turboFactor, engineOutput, rollingResistance);
                if (accelerationForZeroThrottle > requiredAcceleration) {
                    return 0.0;
                }
            }
            var resultThrottle = throttle;
            while (resultAcceleration < requiredAcceleration && resultThrottle < 1.0) {
                resultThrottle += 0.01;
                resultAcceleration = getAccelerationForThrottle(resultThrottle, currentSpeed, turboFactor, engineOutput, rollingResistance);
            }
            return resultThrottle;
        }
    }

    var getThrottle = function(currentSpeed, currentThrottle, turbo, nextCurve, thisCurve, engineOutput, rollingResistance) {
        var getSafeDistanceToCurve = function(curveMaxSpeedVelocity, distanceToCurve, speed) {
            var reducedSpeed = speed;
            var safeDistanceToCurve = distanceToCurve;
            while (safeDistanceToCurve > 0 && reducedSpeed - maxSpeedForCurve > 0) {
                safeDistanceToCurve -= reducedSpeed * 1; // speed is in ticks, and the drag coefficient, too
                var decelerationForZeroThrottle = getAccelerationForThrottle(0, reducedSpeed, turboFactor, engineOutput, rollingResistance);
                reducedSpeed += decelerationForZeroThrottle;
            }
            return safeDistanceToCurve;
        };

        var throttle;
        var turboFactor = 1;
        if (turbo && turbo.startedAt) {
            turboFactor = turbo.data.turboFactor;
        }

        var maxSpeedForCurve = nextCurve.maxSafeVelocity;
        var safeDistanceToNextCurve = getSafeDistanceToCurve(nextCurve.maxSafeVelocity, nextCurve.distance, currentSpeed);

        var nextPieceAfterTheCurve = model.mapPieceInfo[nextCurve.index + 1 % model.mapPieceInfo.length];
        if (nextPieceAfterTheCurve.isTurn) {
            var nextCurveMapPiece = model.mapPieceInfo[nextCurve.index];
            var safeDistanceToCurveAfterTheNextOne = getSafeDistanceToCurve(nextPieceAfterTheCurve.maxSafeVelocity, nextCurve.distance + nextCurveMapPiece.laneLengths[model.myStatus.position.lane.endLaneIndex], currentSpeed);
            if (safeDistanceToCurveAfterTheNextOne < safeDistanceToNextCurve) {
                safeDistanceToNextCurve = safeDistanceToCurveAfterTheNextOne;
            }
        }
 
        throttle = safeDistanceToNextCurve > 0 ? 1.0 : 0.0; // we can slow down later, now just full speed ahead!

        if (throttle == 1.0 && thisCurve) {
            // ...unless this is a curve, in which case we need to maintain the current speed
            maxSpeedForCurve = thisCurve.maxSafeVelocity;            

            throttle = getThrottleForTargetSpeed(thisCurve.maxSafeVelocity, currentSpeed, currentThrottle, turboFactor, engineOutput, rollingResistance);

            // limit the speed in case we are slipping too much
            var slipAngleAbs = Math.abs(model.myStatus.slipAngle);
            if (throttle > 0.3 && (slipAngleAbs > 30 || (model.myStatus.slipAngleChange > 5))) {
                jog.debug("throttle", { msg: "Cutting throttle due to too high slip angle", slipAngle: model.myStatus.slipAngle, slipAngleChange: model.myStatus.slipAngleChange });
                throttle = 0; //0.3 / engineOutput;
            }

            //if (maxSpeedForCurve > currentSpeed) {

            //    while()
            //    throttle = targetThrottle; // should slow down eventually..
            //}

            //var accelerationForCurrentThrottle = 

            //var frictionForceEstimate = model.environmentVariables.frictionForceEstimate;
            //// we are in a curve, so use the centripetal force to see if we can throttle
            //if (currentCentripetalForce < 0.5 * frictionForceEstimate) {
            //    // still safe, full throttle!
            //    throttle = 1.0;
            //} else if (currentCentripetalForce < 0.7 * frictionForceEstimate) {
            //    // nearing the limit -> just keep it there...
            //    throttle = 0.65; // TODO: we really should calculate the optimal throttle based on the speed..
            //} else if (currentCentripetalForce < 0.80 * frictionForceEstimate) {
            //    // at the limit - > slight slow down
            //    throttle = 0.5; // TODO: we really should calculate the optimal throttle based on the speed..
            //} else {
            //    // whoah! going to crash! abort abort!
            //    throttle = 0.0;
            //}

            //if (turbo && turbo.stillOnForTicks > 0 && throttle > 0.5) {
            //    throttle = 0.5; // turbo on -> not safe to throttle too much!
            //}         
        }

        if (model.constantThrottle) {
            throttle = model.constantThrottle;
        }

        if (throttle > 1.0) {
            throttle = 1.0;
        }

        jog.debug("throttle", {
            throttle: throttle,
            turbo: turbo,
            curve: thisCurve ? thisCurve : nextCurve,
            maxSpeedForCurve: maxSpeedForCurve,
        });

        return throttle;
    };

    var thisCurveInfo = model.getThisCurveInfo();
    var nextCurveInfo = model.getNextCurveInfo();    

    result.throttle = getThrottle(model.myStatus.velocity, model.myStatus.previousThrottle, model.myStatus.turbo, nextCurveInfo, thisCurveInfo, model.environmentVariables.engineOutput, model.environmentVariables.rollingResistanceCoefficient);
    result.estimatedAccelerationForThrottle = getAccelerationForThrottle(result.throttle, model.myStatus.velocity, model.environmentVariables.engineOutput, 1, model.environmentVariables.rollingResistanceCoefficient);
    return result;
};
