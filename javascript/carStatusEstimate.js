﻿var degToRad = function (deg) {
    return deg * Math.PI / 180;
};

var radToDeg = function (rad) {
    return rad * 180 / Math.PI;
};

var CarStatusEstimate = (function () {
    function CarStatusEstimate(mapPieces, position, currentVelocity, currentSlipAngle, currentSlipAngleVelocity, turboOnForTicks, turboFactor, environmentVariables) {
        this.mapPieces = mapPieces;
        this.position = position;
        this.currentVelocity = currentVelocity;
        this.currentSlipAngle = currentSlipAngle;
        this.currentSlipAngleVelocity = currentSlipAngleVelocity;
        this.turboOnForTicks = turboOnForTicks;
        this.turboFactor = turboFactor;
        this.environmentVariables = environmentVariables;
        this.slipAngles = [];
    }
    CarStatusEstimate.prototype.updateOneStep = function (throttle) {
        var currentPiece = this.mapPieces[this.position.pieceIndex];
        var radius = currentPiece.isTurn ? currentPiece.radii[this.position.lane.endLaneIndex] : 0;
        this.currentPieceAngle = currentPiece.angle;

        var alpha = -0.00125 * this.currentVelocity * this.currentSlipAngle - 0.1 * this.currentSlipAngleVelocity;
        var torque = 0;
        if (currentPiece.isTurn && this.currentVelocity > (0.3 / this.environmentVariables.curveCoefficient) * Math.sqrt(radius)) {
            if (currentPiece.isLeftTurn) {
                torque = (-this.environmentVariables.curveCoefficient / Math.sqrt(radius)) * (this.currentVelocity * this.currentVelocity) + 0.3 * this.currentVelocity;
            } else {
                torque = (this.environmentVariables.curveCoefficient / Math.sqrt(radius)) * (this.currentVelocity * this.currentVelocity) - 0.3 * this.currentVelocity;
            }
        }

        alpha += torque;
        this.currentSlipAngleVelocity += alpha;
        this.currentSlipAngle += this.currentSlipAngleVelocity;
        this.slipAngles.push(this.currentSlipAngle);

        var tf = 1;
        if (this.turboOnForTicks > 0) {
            tf = this.turboFactor;
            this.turboOnForTicks--;
        }
        var acceleration = this.environmentVariables.engineOutput * throttle * tf - this.environmentVariables.rollingResistanceCoefficient * this.currentVelocity;

        this.currentVelocity += acceleration;

        var newPosition = {
            pieceIndex: this.position.pieceIndex,
            inPieceDistance: this.position.inPieceDistance + this.currentVelocity,
            lane: {
                startLaneIndex: this.position.lane.startLaneIndex,
                endLaneIndex: this.position.lane.endLaneIndex
            },
            lap: this.position.lap
        };

        var currentLaneLength = currentPiece.laneLengths[this.position.lane.endLaneIndex];
        if (newPosition.inPieceDistance >= currentLaneLength) {
            newPosition.inPieceDistance -= currentLaneLength;
            newPosition.pieceIndex = (newPosition.pieceIndex + 1) % this.mapPieces.length;
            if (newPosition.pieceIndex == 0) {
                newPosition.lap++;
            }
        }

        this.position = newPosition;
    };
    return CarStatusEstimate;
})();
exports.CarStatusEstimate = CarStatusEstimate;