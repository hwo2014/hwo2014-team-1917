﻿var prnt = require('sprintf.js');
var colors = require('colors');

var argv = require('yargs').argv;
var consoleTracking = argv.consoleTracking;

var consoleHeadersWritten = false;

exports.logStatus = function (status) {
    if (!consoleHeadersWritten) {
        process.stdout.write("|  tick | velocity | aclr |  angle | angEst | angDiff | angVel | thr |  position  | lane |\n");
        consoleHeadersWritten = true;
    }
    if (consoleTracking) {
        var s = status;
        process.stdout.write('\033[0G');
        process.stdout.write(sprintf("| %5d |%10.2f|%6.3f| %7.4f | %7.4f | %6.4f | %6.4f | %3.2f | %d:%02d:%05.2f | %4d |",
            s.tick, s.velocity, s.acceleration,
            s.slipAngle, s.slipAngleEstimate, s.slipAngleDiffFromEstimate, s.slipAngleVelocity, s.throttle,
            s.position.lap, s.position.pieceIndex, s.position.inPieceDistance, s.position.lane.endLaneIndex));
    }
};

exports.logRaceEnd = function(results) {
    console.log("\nResults: %j".yellow, results);
}

exports.logCrash = function(crashMsg) {
    console.log("\nCRASH! %j".redBG, crashMsg);
}

exports.logTurbo = function(turbo) {
    console.log("\nTURBO BOOST! Tsojojojoing! %j".bold, turbo);
}

exports.logEnvironmentCalculation = function(variable, value) {
    console.log("\nCalculated %s: %d".grey, variable, value);
}