﻿var _ = require('underscore');
var physicsCalculator = require('./physicsCalculator');
var jog = require('./logWrapper');
var competitorTracker = require('./competitorTracker');
var consoleLogger = require('./consoleLogger');
var c = require("./carStatusEstimate");

var degToRad = function(deg) {
    return deg * Math.PI / 180;
}

var radToDeg = function(rad) {
    return rad * 180 / Math.PI;
}

exports.initModel = function() {
    var self = this;

    self.model = {
        race: {},
        carPositions: {},
        lastTick: 0,
        myCar: {},
        mapPieceInfo: [],
        competitorInfo: {},
        myStatus: {
            velocity: 0,
            acceleration: 0,
            position: {
                pieceIndex: 0,
                inPieceDistance: 0.0,
                lane: {
                    startLaneIndex: 0,
                    endLaneIndex: 0
                }
            },
            lap: 0,
            throttle: 0,
            previousThrottle: 0,
            raceStarted: false,
            crashed: false,
            spawned: false
        },
        environmentVariables: {
            corneringSpeedCoefficient: 0.000786,
            frictionForceEstimate: 0.58,
            rollingResistanceCoefficient: 0.015,
            engineOutput: 1,
            curveCoefficient: 0.5303
        },
        lapStatistics: [
            {
                maxAngleInCorners: 0.0
            }
        ],
        lastPieceStatistics: []
    };

    self.model.getThisCurveInfo = function() {
        var currentMapPiece = this.mapPieceInfo[this.myStatus.position.pieceIndex];
        if (currentMapPiece.isTurn) {
            var endLaneIndex = this.myStatus.position.lane.endLaneIndex;
            var difficulty = currentMapPiece.difficulties[endLaneIndex];
            var radius = currentMapPiece.radii[endLaneIndex];
            var maxSafeVelocity = currentMapPiece.maxSpeedLimitSet ? currentMapPiece.maxVelocity[endLaneIndex] : getMaxSpeedForCurve(radius);
            return {
                distance: 0,
                radius: radius,
                difficulty: difficulty,
                maxSafeVelocity: maxSafeVelocity
            }
        }

        return null; // return null so it is definitely falsey
    }

    var getMaxSpeedForCurve = function(radius) {
        // tries to figure out the maximum speed for a curve based on the friction force estimate        
        return Math.sqrt(self.model.environmentVariables.frictionForceEstimate * radius);
    };

    self.model.getNextCurveInfo = function() {
        var myPosition = self.model.myStatus.position;
        var mapPieceInfo = self.model.mapPieceInfo;
        var laneIndex = myPosition.lane.endLaneIndex;
        // todo: take lane switches during curve into account
        var distance = mapPieceInfo[myPosition.pieceIndex].laneLengths[laneIndex] - myPosition.inPieceDistance;

        var i = 1; // relative index to current mapPiece
        var mapPiece = mapPieceInfo[(myPosition.pieceIndex + i) % mapPieceInfo.length]; // map piece to consider
        // stop if we have looped over all the items or the map piece is a turn
        while (i < mapPieceInfo.length && !mapPiece.isTurn) {
            // map piece is not a turn add length to distance and check the next one
            distance += mapPiece.laneLengths[laneIndex];
            i++;
            mapPiece = mapPieceInfo[(myPosition.pieceIndex + i) % mapPieceInfo.length];
        }
        
        var radius = mapPiece.radii[laneIndex];
        var maxSafeVelocity = mapPiece.maxSpeedLimitSet ? mapPiece.maxVelocity[laneIndex] : getMaxSpeedForCurve(radius);
        return {
            distance: distance,
            radius: radius,
            difficulty: mapPiece.difficulties[laneIndex],
            maxSafeVelocity: maxSafeVelocity,
            index: mapPiece.pieceIndex
        }
    };

    self.model.getLaneLength = function(pos) {
        var mapPiece = this.mapPieceInfo[pos.pieceIndex];
        if (pos.lane.startLaneIndex != pos.lane.endLaneIndex) {
            // lane was changed, need to take into account in distance calculations! // TODO: move to getMapPieceInfo

            if (mapPiece.isTurn) {
                // switches in turns are difficult to calculate, lets just estimate as the longest lane
                return _.max(mapPiece.laneLengths);
            } else {
                // damn, the simple diagonal length does not seem to be exact
                // so we could just use the const from reddit: http://www.reddit.com/r/HWO/comments/23ye82/lane_crossing_curve_specs/
                // BUT: this does not work for the switches in corners!
                // TODO: calculate the speed from the throttle -> speed function
                return 102.06;
            }

            var straightLaneLength = mapPiece.laneLengths[pos.lane.endLaneIndex]; 
            var distanceBetweenLanes = this.race.track.lanes[pos.lane.startLaneIndex].distanceFromCenter - this.race.track.lanes[pos.lane.endLaneIndex].distanceFromCenter;
            var diagonalLaneLength = Math.sqrt(Math.pow(distanceBetweenLanes, 2) + Math.pow(straightLaneLength, 2));
            jog.debug("map", {
                msg: "checked lane length after lane switch, result: " + diagonalLaneLength,
                straightLaneLength: straightLaneLength,
                distanceBetweenLines: distanceBetweenLanes,
                diagonalLaneLength: diagonalLaneLength
            });
            return diagonalLaneLength; // c^2 = a^2 + b^2
        } else {
            return mapPiece.laneLengths[pos.lane.endLaneIndex];
        }
    }

    self.model.calculateSpeed = function(newPos) {
        var prevPos = this.previousStatus.position;
        var distanceTraveled = -1;
        if (prevPos.pieceIndex != newPos.pieceIndex) {
            var prevLaneLength = this.getLaneLength(prevPos);
            distanceTraveled = prevLaneLength - prevPos.inPieceDistance + newPos.inPieceDistance;
        } else {
            distanceTraveled = newPos.inPieceDistance - prevPos.inPieceDistance;
        }

        return distanceTraveled;
    };

    self.model.calculateAngularVelocityFromCurve = function(newPos) {
        var getAngleForTraveledDistance = function(pos, distance) {
            var mapPiece = self.model.mapPieceInfo[pos.pieceIndex];
            if (mapPiece.IsTurn) {
                return distance / (2 * Math.PI * mapPiece.radii[pos.lane.endLaneIndex]);
            } else {
                return 0.0; // straight -> no angular velocity
            }
        }

        var prevPos = this.previousStatus.position;
        var angularVelocity;
        if (prevPos.pieceIndex != newPos.pieceIndex) {
            var prevLaneLength = this.getLaneLength(prevPos);
            angularVelocity = getAngleForTraveledDistance(prevPos, prevLaneLength - prevPos.inPieceDistance) + getAngleForTraveledDistance(newPos, newPos.inPieceDistance);
        } else {
            angularVelocity = getAngleForTraveledDistance(newPos, newPos.inPieceDistance - prevPos.inPieceDistance);
        }

        return angularVelocity;
    }

    self.model.updateMaxSpeedData = function(stats) {
        if (!stats || stats.length == 0) {
            jog.warn("cornering", { msg: "Empty stats passed in, cannot update max speed!" });
            return;
        }
        var pos = stats[0].position; // the first stat should always be set
        //var mapPiece = this.mapPieceInfo[pos.pieceIndex];
        //var lane = pos.lane.startLaneIndex;

        var crashes = _.find(stats, function(el) {
            return el.crashed === true;
        });

        var highestSlipAngle = _.max(_.pluck(stats, "angle"), function(a) { return Math.abs(a); });
        var highestVelocityForCorner = _.max(_.pluck(stats, "velocity"), function(v) { return Math.abs(v); });

        var mapPiece = this.mapPieceInfo[pos.pieceIndex];
        var prevPieceIndex = pos.pieceIndex - 1;
        if (prevPieceIndex < 0) {
            prevPieceIndex = mapPiece.length - 1;
        }

        var prevMapPiece = this.mapPieceInfo[prevPieceIndex];

        if (crashes && mapPiece.isTurn){
            if (pos.lap < 1) {
                // reduce cornering speed and mark the value as a high limit TODO: what if it was some other reason that caused the car to crash?
                this.environmentVariables.frictionForceEstimate = 0.95 * this.environmentVariables.frictionForceEstimate;
                consoleLogger.logEnvironmentCalculation("frictionForceEstimateChange", this.environmentVariables.frictionForceEstimate);

                // re-calculate the rolling resistance just in case it is off
                this.environmentVariables.rollingResistanceCalculated = false;

                //var newMaxVelocity = 0.98 * mapPiece.maxVelocity[lane];
                //if (!mapPiece.maxVelocityLimit) {
                //    // init null array if needed
                //    mapPiece.maxVelocityLimit = _.map(mapPiece.lanes, function() {
                //        return null; 
                //    });
                //}

                //mapPiece.maxVelocityLimit[lane] = newMaxVelocity;

                jog.info("cornering", {
                    msg: "Oh noes! Car crashed! Speed limit for last corner was apparently too high. Setting new friction force estimate to: " + this.environmentVariables.frictionForceEstimate,
                    pieceIndex: pos.pieceIndex,
                    lap: pos.lap,
                    lane: pos.lane.startLaneIndex,
                    frictionForceEstimate: this.environmentVariables.frictionForceEstimate
                });
            }
            // the corner may be a special case: just lower its speed, as well as the previous piece
            prevMapPiece.maxVelocity[pos.lane.endLaneIndex] = 0.92 * prevMapPiece.maxVelocity[pos.lane.endLaneIndex];
            prevMapPiece.maxSpeedLimitSet = true;
            consoleLogger.logEnvironmentCalculation("maxSpeedLimit changed for " + prevPieceIndex, prevMapPiece.maxVelocity[pos.lane.endLaneIndex]);

            mapPiece.maxVelocity[pos.lane.endLaneIndex] = 0.90 * highestVelocityForCorner;
            mapPiece.maxSpeedLimitSet = true;
            consoleLogger.logEnvironmentCalculation("maxSpeedLimit changed for " + pos.pieceIndex, mapPiece.maxVelocity[pos.lane.endLaneIndex]);
            
        } else if (highestSlipAngle > 52 && mapPiece.isTurn) {
            if (pos.lap < 1) {
                // reduce cornering speed and mark the value as a high limit
                this.environmentVariables.frictionForceEstimate = 0.98 * this.environmentVariables.frictionForceEstimate;
                consoleLogger.logEnvironmentCalculation("frictionForceEstimateChange", this.environmentVariables.frictionForceEstimate);

                jog.info("cornering", {
                    msg: "Car almost slipped out, reducing the friction force estimate to: " + this.environmentVariables.frictionForceEstimate,
                    pieceIndex: pos.pieceIndex,
                    lap: pos.lap,
                    lane: pos.lane.startLaneIndex,
                    frictionForceEstimate: this.environmentVariables.frictionForceEstimate
                });
            }
            // the corner may be a special case: just lower its speed, as well as the previous piece
            prevMapPiece.maxVelocity[pos.lane.endLaneIndex] = 0.99 * prevMapPiece.maxVelocity[pos.lane.endLaneIndex];
            prevMapPiece.maxSpeedLimitSet = true;
            consoleLogger.logEnvironmentCalculation("maxSpeedLimit changed for " + prevPieceIndex, prevMapPiece.maxVelocity[pos.lane.endLaneIndex]);

            mapPiece.maxVelocity[pos.lane.endLaneIndex] = 0.98 * highestVelocityForCorner;
            mapPiece.maxSpeedLimitSet = true;
            consoleLogger.logEnvironmentCalculation("maxSpeedLimit changed for " + pos.pieceIndex, mapPiece.maxVelocity[pos.lane.endLaneIndex]);
        }

        //if ( mapPiece.frictionForceEstimateLimit) {

        //    mapPiece.maxVelocity[lane] = mapPiece.maxVelocityLimit[lane];
        //    return;
        //}

        //if (!mapPiece.isTurn) {
        //    jog.debug("cornering", { msg: "piece is not a corner, infinite max speed!", pieceIndex: pos.pieceIndex });
        //    return;
        //}

        //// calculate the max drift, and see if it safe to increase the max value
        //var maxSlidesAtNearMaxSpeed = _.chain(stats)
        //    .map(function(s) {
        //        if (stats.difficulty <= 1 / ( 200 * 200 ) ) {
        //            return null; // ignore too easy corners
        //        }
        //        var speedLimitForPiece = mapPiece.maxVelocity[lane];
        //        var maxSpeedDifference = Math.abs(s.velocity - speedLimitForPiece) / speedLimitForPiece;
        //        if(maxSpeedDifference < 0.20) {
        //            return Math.abs(s.angle);
        //        } else {
        //            return null;
        //        }
        //    }).filter( function ( el ) {
        //        return el != null;
        //    }).value();

        //if ( maxSlidesAtNearMaxSpeed.length === 0 ) {
        //    jog.debug("cornering", { msg: "Not enough max speed cornerings to adjust the coefficient." });
        //    return;
        //}
        //var maxSlideAtNearMaxSpeed = _.max(maxSlidesAtNearMaxSpeed);

        //var updatedMaxVelocity;
        //if (maxSlideAtNearMaxSpeed < 10) {
        //    updatedMaxVelocity = 1.20 * mapPiece.maxVelocity[lane];
        //} else if ( maxSlideAtNearMaxSpeed < 30 ) {
        //    updatedMaxVelocity = 1.10 * mapPiece.maxVelocity[lane];
        //} else if ( maxSlideAtNearMaxSpeed < 40 ) {
        //    updatedMaxVelocity = 1.3 * mapPiece.maxVelocity[lane];
        //} else if ( maxSlideAtNearMaxSpeed < 50 ) {
        //    updatedMaxVelocity = 1.01 * mapPiece.maxVelocity[lane];
        //}

        //if (updatedMaxVelocity) {
        //    jog.info("cornering", {
        //        msg: "Speed limit for last corner was apparently too low. Going faster next time!",
        //        pieceIndex: pos.pieceIndex,
        //        lap: pos.lap,
        //        lane: pos.lane.startLaneIndex,
        //        maxSlipAngle: maxSlideAtNearMaxSpeed,
        //        updatedMaxVelocity: updatedMaxVelocity
        //    });
        //    mapPiece.maxVelocity[lane] = updatedMaxVelocity;
        //}
    }

    self.model.reportCrash = function() {
        // clear the possible stored turbo, as expires after a crash
        this.turbo = null;

        self.model.incrementCurveCoefficient();

        if (this.lastPieceStatistics.length > 0) {            
            var lastStat = this.lastPieceStatistics[this.lastPieceStatistics.length - 1];
            lastStat.crashed = true;
        }
    };

    self.model.incrementCurveCoefficient = function() {
        // reduce cornering speed and mark the value as a high limit TODO: what if it was some other reason that caused the car to crash?
        this.environmentVariables.curveCoefficient = 1.01 * this.environmentVariables.curveCoefficient;
        consoleLogger.logEnvironmentCalculation("curveCoefficient", this.environmentVariables.curveCoefficient);

        // re-calculate the rolling resistance just in case it is off
        this.environmentVariables.rollingResistanceCalculated = false;

        jog.info("cornering", {
            msg: "Oh noes! Car crashed! Speed limit for last corner was apparently too high. Setting new curve coefficient to: " + this.environmentVariables.curveCoefficient,
            curveCoefficient: this.environmentVariables.curveCoefficient
        });
    };

    self.model.updatePieceStatistics = function(prevPos, currentPos, velocity, angle) {
        if(prevPos.pieceIndex != currentPos.pieceIndex){
            // we're just out of a corner, see if we need to update any co-efficients
            this.updateMaxSpeedData( this.lastPieceStatistics );
            this.lastPieceStatistics.length = 0; // reset so the old stats will not be used again
        }

        // add the new piece stats
        self.model.lastPieceStatistics.push({
            position: currentPos, 
            angle: angle,
            velocity: velocity,
        });
    };

    var throttleOffForTicks = 0;
    self.model.updateEnvironmentVariablesIfNotSet = function(previousStatus, currentVelocity) {
        if (this.environmentVariables.rollingResistanceCalculated && this.environmentVariables.engineOutputCalculated && this.environmentVariables.maxSlipForceCalculated && this.environmentVariables.curveCoefficientCalculated) {
            return;
        }

        var mapPiece = this.mapPieceInfo[previousStatus.position.pieceIndex];

        if (!this.environmentVariables.curveCoefficientCalculated && previousStatus.slipAngleVelocity !== 0) {
            if (mapPiece.isTurn) {
                var radius = mapPiece.radii[previousStatus.position.lane.endLaneIndex];
                var newCurveCoefficient = 0;
                if (mapPiece.isLeftTurn) {
                    newCurveCoefficient = -(previousStatus.slipAngleVelocity - 0.3 * currentVelocity) * Math.sqrt(radius) / (currentVelocity * currentVelocity);
                } else {
                    newCurveCoefficient = (previousStatus.slipAngleVelocity + 0.3 * currentVelocity) * Math.sqrt(radius) / (currentVelocity * currentVelocity);
                }
                if (newCurveCoefficient > 0.5 && newCurveCoefficient < 0.6) {
                    this.environmentVariables.curveCoefficient = 1.02 * newCurveCoefficient; //increment the coefficient a bit, to be on the safe side
                    this.environmentVariables.curveCoefficientCalculated = true;
                    consoleLogger.logEnvironmentCalculation("curveCoefficient", this.environmentVariables.curveCoefficient);
                    jog.info("physics", { msg: "Calculated curve coefficient: " + this.environmentVariables.curveCoefficient, curveCoefficient: this.environmentVariables.curveCoefficient });
                }
            }
        }

        if (!this.environmentVariables.maxSlipForceCalculated && previousStatus.slipAngle > 0) {
            if (!previousStatus || !previousStatus.position) {
                jog.debug("physics", { msg: "Cannot calculate max slip force, previous status not set"});
                return;
            }
            // http://www.reddit.com/r/HWO/comments/245yms/how_to_calculate_speed_at_which_you_start_slipping/
            if (!mapPiece) {
                jog.debug("physics", { msg: "Cannot calculate max slip force, map piece not found for position " + previousStatus.position.pieceIndex });
                return;
            }
            if (mapPiece.isTurn) {
                var laneRadius = mapPiece.radii[previousStatus.position.lane.endLaneIndex];
                var angularVelocityForSlip = radToDeg(previousStatus.velocity / laneRadius) - previousStatus.slipAngle;
                var thresholdSpeed = degToRad(angularVelocityForSlip) * laneRadius;
                this.environmentVariables.maxSkipForce = (thresholdSpeed * thresholdSpeed / laneRadius) * 1.1; // Add some more speed!
                this.environmentVariables.maxSlipForceCalculated = true;

                if (this.environmentVariables.frictionForceEstimate > this.environmentVariables.maxSkipForce &&
                    this.environmentVariables.frictionForceEstimate - 0.1 < this.environmentVariables.maxSkipForce ) { // do not go over the limits, please
                    this.environmentVariables.frictionForceEstimate = this.environmentVariables.maxSkipForce;
                    consoleLogger.logEnvironmentCalculation("frictionForceEstimate", this.environmentVariables.frictionForceEstimate);                 
                    jog.info("physics", { msg: "Calculated friction force estimate: " + this.environmentVariables.frictionForceEstimate, frictionForceEstimate: this.environmentVariables.frictionForceEstimate });
                }
            }
        }

        if (!this.environmentVariables.engineOutputCalculated && previousStatus.velocity == 0 && currentVelocity > 0 && previousStatus.throttle > 0) {
            // ok, the engine output is just the current velocity
            this.environmentVariables.engineOutput = currentVelocity / previousStatus.throttle;
            consoleLogger.logEnvironmentCalculation("engineOutput", this.environmentVariables.engineOutput);
            jog.info("physics", { msg: "Calculated engine output: " + this.environmentVariables.engineOutput, engineOutput: this.environmentVariables.engineOutput});
            this.environmentVariables.engineOutputCalculated = true;
        }

        if (!previousStatus.throttle || previousStatus.throttle == 0) {
            throttleOffForTicks++;
        } else {
            throttleOffForTicks = 0;
        }

        if (throttleOffForTicks > 2 && !this.environmentVariables.rollingResistanceCalculated) {
            jog.debug("physics", { msg: "Calculating rolling resistance", previousVelocity: previousStatus.velocity, currentVelocity: currentVelocity });
            var rollingResistanceCoefficient = (previousStatus.velocity - currentVelocity) / previousStatus.velocity;
            if (!isNaN(rollingResistanceCoefficient) && 
                rollingResistanceCoefficient < 0.03 && rollingResistanceCoefficient > 0.01 
                && this.environmentVariables.rollingResistanceCoefficient != rollingResistanceCoefficient) {
                jog.info("physics", {
                    msg: "Rolling resistance coefficient has changed to " + rollingResistanceCoefficient,
                    rollingResistanceCoefficient: rollingResistanceCoefficient
                });
                this.environmentVariables.rollingResistanceCoefficient = rollingResistanceCoefficient;
                this.environmentVariables.rollingResistanceCalculated = true;

                consoleLogger.logEnvironmentCalculation("rollingResistanceCoefficient", this.environmentVariables.rollingResistanceCoefficient);
            }
        }
    };

    self.model.updateStatus = function(positions, tick) {
        self.model.competitorInfo = competitorTracker.updateData(self.model.myCar.name, positions, this.mapPieceInfo);

        // store the previous status
        this.previousStatus = this.myStatus;

        var myNewStatus = _.find(positions, function(el) {
            return el.id.name === self.model.myCar.name;
        });

        var pos = myNewStatus.piecePosition;
        var velocity = this.calculateSpeed(pos);
        var acceleration = velocity - this.previousStatus.velocity;
        
        this.updateEnvironmentVariablesIfNotSet(this.previousStatus, velocity);

        physicsCalculator.addSpeedData(tick, velocity, acceleration, this.previousStatus.throttle, this.turbo);
        var mapPiece = this.mapPieceInfo[pos.pieceIndex];
        var centripetalForce = mapPiece.isTurn ? 1 * (velocity * velocity) / mapPiece.radii[pos.lane.endLaneIndex] : 0;

        //this.updatePieceStatistics(this.previousStatus.position, pos, velocity, myNewStatus.angle);

        var baseAngularVelocity = this.calculateAngularVelocityFromCurve(pos);
        var angularVelocity = ((myNewStatus.angle - this.previousStatus.slipAngle) * Math.PI / 180) + baseAngularVelocity;
        var angularAcceleration = angularVelocity - this.previousStatus.angularVelocity;
        
        var nextCurve = this.getNextCurveInfo();

        if (this.turbo && this.turbo.startedAt) {
            this.turbo.stillOnForTicks = this.turbo.startedAt - tick + this.turbo.data.turboDurationTicks;
            jog.debug("turbo", { msg: "turbo still on for " + this.turbo.stillOnForTicks + "ticks", turbo: this.turbo });
        }
        
        this.clearForTurbo = !mapPiece.isTurn && (nextCurve.distance > 350 || nextCurve.distance >= self.model.maxStraightLength);
        if (this.turbo && this.clearForTurbo) {
            jog.info("turbo", { msg: "All clear for turbo!", isTurn: mapPiece.isTurn, distanceToNextCurve: nextCurve.distance });
        }

        var slipAngleVelocity = myNewStatus.angle - this.previousStatus.slipAngle;

        var estimate = new c.CarStatusEstimate(this.mapPieceInfo, pos, velocity, myNewStatus.angle, slipAngleVelocity, null, 1, this.environmentVariables);
        estimate.updateOneStep(this.myStatus.throttle);

        var slipAngleChange = myNewStatus.angle - this.previousStatus.slipAngle;
        var pieceRadius = mapPiece.isTurn ? mapPiece.radii[pos.lane.endLaneIndex] : 0;
        var slipAngleVelocityRad = (this.previousStatus.slipAngle - myNewStatus.angle) * Math.PI / 180;
        var slipAngleAccelerationRad = this.previousStatus.slipAngleVelocityRad - slipAngleVelocityRad;
                

        //if(mapPiece.isTurn && this.previousStatus.pieceRadius != pieceRadius) {
        //    var estimatedAdditionalAngleAcceleration = centripetalForce * mapPiece.angle * Math.PI / 180;
        //    var previousSlipAngleAcceleration = this.previousStatus.slipAngleAccelerationRad * 180 / Math.PI;
        //    var currentSlipAngleAcceleration = slipAngleAccelerationRad * 180 / Math.PI;
        //    console.log("\nEstimated extra slipAngleAcceleration %d, was %d (total: %d)", estimatedAdditionalAngleAcceleration, currentSlipAngleAcceleration - previousSlipAngleAcceleration, currentSlipAngleAcceleration);
        //}        

        this.myStatus = {
            tick: tick,
            velocity: velocity,
            acceleration: acceleration,
            accelerationDiffFromEstimate: acceleration - this.previousStatus.estimatedAccelerationForThrottle,
            centripetalForce: centripetalForce,
            position: myNewStatus.piecePosition,
            carPositions: positions,
            clearForTurbo: this.clearForTurbo,
            turbo: this.turbo,
            isTurn: mapPiece.isTurn,
            previousThrottle: this.previousStatus.throttle,               
            secondToLastThrottle: this.previousStatus.previousThrottle,         
            estimatedAccelerationForSecondToLastThrottle: this.previousStatus.estimatedAccelerationForThrottle,
            nextCurveDistance: nextCurve.distance,
            slipAngle: myNewStatus.angle,
            slipAngleEstimate: this.previousStatus.slipAngleEstimateForNextStep,
            slipAngleEstimateForNextStep: estimate.currentSlipAngle,
            slipAngleDiffFromEstimate: this.previousStatus.slipAngleEstimate - myNewStatus.angle,
            slipAngleVelocity: slipAngleVelocity,
            slipAngleVelocityRad: slipAngleVelocityRad,
            slipAngleAccelerationRad: slipAngleAccelerationRad,
            pieceAngle: mapPiece.angle || 0,
            pieceRadius: pieceRadius,
            slipAngleChange: slipAngleChange,
            angularVelocity: angularVelocity, // in rad
            angularAcceleration: angularAcceleration // in rad/s^2
        }
    }

    self.model.initCar = function(data) {
        self.model.myCar = data;
    }

    self.model.initRace = function(data) {
        var getMaxStraightLength = function(pieces) {
            var currentMax = 0;
            var currentStraightLength = 0;
            _.each(pieces, function(p) {
                if (!p.isTurn) {
                    currentStraightLength += p.laneLengths[0];
                    if (currentStraightLength > currentMax) {
                        currentMax = currentStraightLength;
                    }
                } else {
                    currentStraightLength = 0;
                }
            });
            return currentMax;
        };
        self.model.race = data.race; //TODO: build the actual model
        self.model.mapPieceInfo = self.model.getMapPieceInfo(data.race.track.pieces, data.race.track.lanes);
        self.model.maxStraightLength = getMaxStraightLength(self.model.mapPieceInfo);        
        jog.debug("map", { maxStraightLength: self.model.maxStraightLength, map: self.model.mapPieceInfo });
    };

    //TODO: couldn't this be made private?
    self.model.getMapPieceInfo = function(mapPieces, lanes) {
        return _.map(mapPieces, function(mapPiece, key) {
            var result = {};
            result.pieceIndex = key;
            result.isTurn = !_.isUndefined(mapPiece.radius);
            result.isLeftTurn = mapPiece.angle < 0; // a negative angle means a left turn
            result.hasSwitch = mapPiece.switch || false;
            result.lanes = lanes;
            result.laneLengths = _.map(lanes, function(lane,key) {
                if (result.isTurn) {
                    // length traveled is the 2 * pi * r * angle(degrees) / 360
                    // if we are turning left, negative distanceFromCenter is more to the left (closer to the inner curve)  
                    var distanceFromCenter = lane.distanceFromCenter * (result.isLeftTurn ? -1 : 1);
                    var length = 2 * Math.PI * (mapPiece.radius - distanceFromCenter) * mapPiece.angle / 360;
                    var value = Math.abs(length);
                    return value;
                } else {
                    // length traveled is always the length
                    return mapPiece.length;
                }
            });

            if (result.isTurn) {
                result.angle = mapPiece.angle;
                result.radii = _.map(lanes, function(lane) {
                    return result.isLeftTurn
                        ? mapPiece.radius + lane.distanceFromCenter //TODO: take the drive direction (clockwise/counterclockwise)also into account
                        : mapPiece.radius - lane.distanceFromCenter;
                });
                result.difficulties = _.map(result.radii, function(r) {
                    return 1 / (r * r);
                });

                result.maxVelocity = _.map(result.radii, function(laneRadius) {
                    return getMaxSpeedForCurve(laneRadius);
                });
            } else {
                result.maxVelocity = _.map(lanes, function() {
                    return Math.Infinity; // you can to really fast on straigts!
                });
            }

            return result;
        });
    };


    return self.model;
};